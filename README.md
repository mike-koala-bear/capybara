<div align="center">
  <h1>Capybara</h1>
  <h2>A fun capybara game!</h2>

  ![version](https://img.shields.io/badge/version-1.0.0-blue)
  ![Static Badge](https://img.shields.io/badge/tests-passing-yellow)
  ![GitHub commit activity (branch)](https://img.shields.io/github/commit-activity/t/mike-koala-bear/capybara/main?color=%23ed4523)
  ![GitHub issues](https://img.shields.io/github/issues/mike-koala-bear/capybara?color=%234123ed)
  ![Github pull requests](https://img.shields.io/github/issues-pr/mike-koala-bear/capybara?color=%23ed23ce)
</div>

## Intro
More coming soon...

### Features
- 🧑‍💻 Play capybara
- 🔥 Learn new words
