# frozen_string_literal: true

class GamesController < ApplicationController
  before_action :set_game, only: %i[show edit update destroy check]

  # GET /games or /games.json
  def index
    @games = Game.all
  end

  # GET /games/1 or /games/1.json
  def show
    word_length = @game.word.length
    @word_array = @game.word.split('')
    @hidden_word = Array.new(word_length, '*').join('')
  end

  # GET /games/new
  def new
    @auto_game = true if params[:auto] == 'true'
    @game = Game.new
  end

  # GET /games/1/edit
  def edit; end

  def check
    @word = @game.word
    @used_letters = session[:used_letters] || []
    letter = params[:letter]
    word_length = @game.word.length
    @word_array = @game.word.split('')

    # Retrieve @hidden_word_array from session if it exists, otherwise initialize a new one
    @hidden_word_array = session[:hidden_word_array] || Array.new(word_length, '*')
    @hidden_word = @hidden_word_array.join('')

    if @word_array.include?(letter)
      @word_array.each_index do |index|
        if @word_array[index] == letter
          @hidden_word_array[index] = letter
        end
      end

      @hidden_word = @hidden_word_array.join('')
      # Store @hidden_word_array in session
      session[:hidden_word_array] = @hidden_word_array
      @finished_game = finished_game?
    else
      @used_letters << letter unless @used_letters.include?(letter)
      session[:used_letters] = @used_letters
      @used_letters = @used_letters.join(',')

      respond_to do |format|
        format.html { redirect_to game_path } # Redirect if needed
        format.turbo_stream do
          render turbo_stream: turbo_stream.update('used_letters', partial: 'used_letters',
                                                   locals: { used_letters: @used_letters })
        end
      end
    end
  end

  # POST /games or /games.json
  def create
    if params[:game][:auto_game] == 'true'
      random_word = Word.all.sample.name.downcase
      @game = Game.new(word: random_word)
    else
      @game = Game.new(game_params)
    end


    respond_to do |format|
      if @game.save
        format.html { redirect_to game_url(@game), notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1 or /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to game_url(@game), notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1 or /games/1.json
  def destroy
    @game.destroy
    session.delete(:hidden_word_array)
    session.delete(:used_letters)

    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_game
    @game = Game.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def game_params
    params.require(:game).permit(:word)
  end

  def finished_game?
    @word == @hidden_word
  end
end
