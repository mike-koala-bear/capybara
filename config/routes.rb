# frozen_string_literal: true

Rails.application.routes.draw do
  get 'pages/home'
  resources :games do
    post :check, on: :member
  end

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'pages#home'
end
