# frozen_string_literal: true

class AddWordToGames < ActiveRecord::Migration[7.0]
  def change
    add_column :games, :word, :string
  end
end
